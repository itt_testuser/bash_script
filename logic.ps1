$groups = "group1;OU=ittstar,DC=mylab,DC=local;global","group2;OU=ittstar,DC=mylab,DC=local;domainlocal"
foreach ($line in $groups)
{
    $info = $line -split ";"

    $group = $info[0]
    $oupath = $info[1]
    $grouptype = $info[2]

    New-ADGroup -Name $group -Path $oupath -GroupScope $grouptype


}

